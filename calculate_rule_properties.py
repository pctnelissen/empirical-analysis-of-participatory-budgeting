import csv
from os.path import exists
from numpy import mean, median, exp

from pbvoting.instance.pabulib import parse_pabulib
from pbvoting.instance.pbinstance import total_cost
from pbvoting.instance.satisfaction import COST_SATISFACTION, EFFORT_SATISFACTION


def read_results(results_path, pabulib_directory, min_votes=0, min_projects=0):
    data = dict()
    with open(results_path, 'r') as file:
        reader = csv.reader(file, delimiter=';')

        first_line = True
        for row in reader:
            if first_line:
                labels = row[1:]
                first_line = False
            else:
                instance_id = int(row[0])
                pabulib_file = pabulib_directory + '/' + str(instance_id) + '.pb'
                instance, profile = parse_pabulib(pabulib_file)

                v = int(instance.meta["num_votes"])
                p = int(instance.meta["num_projects"])
                if v < min_votes or p < min_projects:
                    print(f"Omitted instance {instance_id} with {v} votes and {p} projects")
                    continue

                dictionary = {
                    "instance": instance,
                    "profile": profile
                }
                for i in range(len(labels)):
                    if row[i + 1] == "":
                        dictionary[labels[i]] = set()
                    else:
                        winners = row[i + 1].split(',')
                        for j in range(len(winners)):
                            winners[j] = find_project(winners[j], instance)
                        dictionary[labels[i]] = set(winners)
                data[instance_id] = dictionary

    return data


def find_project(project_id, instance):
    for project in instance:
        if project.name == project_id:
            return project
    raise Exception(f"Project {project_id} not found in file {instance.file_name}.")


def n_winners(data, rule, instance_id):
    return len(data[instance_id][rule])


def similarity(winners_1, winners_2, weight_func, symmetric=False):
    if symmetric:
        if len(winners_1) + len(winners_2) == 0:
            return 1

        return 2 * weight_func(winners_1 & winners_2) / (weight_func(winners_1) + weight_func(winners_2))
    else:
        if len(winners_1) == 0:
            return 1

        return weight_func(winners_1 & winners_2) / weight_func(winners_1)


def similarity_cost(winners_1, winners_2, symmetric=False):
    return similarity(winners_1, winners_2, total_cost, symmetric=symmetric)


def cost_similarity_to_cost(data, rule, instance_id):
    winners_cost = data[instance_id]["Greedy"]
    winners_rule = data[instance_id][rule]
    return similarity_cost(winners_cost, winners_rule, symmetric=True)


def calculate_all_satisfactions(instance, profile, winners, satisfaction_function, normalize=False):
    satisfactions = []
    for ballot in profile:
        satisfaction = satisfaction_function.sat(instance, ballot, winners)
        if normalize:
            satisfaction /= instance.budget_limit
        satisfactions.append(satisfaction)
    return satisfactions


def avg_cost_satisfaction(data, rule, instance_id, normalize=True):
    instance = data[instance_id]["instance"]
    profile = data[instance_id]["profile"]
    winners = data[instance_id][rule]

    satisfactions = calculate_all_satisfactions(instance, profile, winners, COST_SATISFACTION(instance, profile))

    if normalize:
        return mean(satisfactions) / instance.budget_limit

    return mean(satisfactions)


def ballot_to_category_proportions(instance, ballot, weight_function, all_categories=None):
    projects_per_category = dict()
    for project in ballot:
        category = instance.project_meta[project]["category"]
        if category not in projects_per_category.keys():
            projects_per_category[category] = []
        projects_per_category[category].append(project)

    weight_per_category = dict()
    for category in projects_per_category.keys():
        category_weight = weight_function(instance, ballot, projects_per_category[category])
        weight_per_category[category] = category_weight
    total_weight = sum(weight_per_category.values())

    proportions = dict()
    for category in weight_per_category.keys():
        proportions[category] = weight_per_category[category] / total_weight

    if all_categories is not None:
        for category in all_categories:
            if category not in proportions.keys():
                proportions[category] = 0

    return proportions


def calculate_proportions_per_rule(instance, profile, weight_function, rules, data, instance_id, all_categories):
    proportions_per_rule = dict()

    all_proportions = []
    for ballot in profile:
        proportions = ballot_to_category_proportions(instance, ballot, weight_function)
        all_proportions.append(proportions)

    avg_weight_per_category = dict()
    for category in all_categories:
        s = 0
        for proportions in all_proportions:
            if category in proportions.keys():
                s += proportions[category]
        s /= len(all_proportions)
        avg_weight_per_category[category] = s
    proportions_per_rule["Voters"] = avg_weight_per_category

    for rule in rules:
        winners = data[instance_id][rule]
        proportions = ballot_to_category_proportions(instance, winners, weight_function,
                                                     all_categories=all_categories)
        proportions_per_rule[rule] = proportions

    return proportions_per_rule


def category_disproportionality(data, rule, instance_id, invert=False):
    instance = data[instance_id]["instance"]
    profile = data[instance_id]["profile"]

    if "category" not in instance.project_meta[list(instance)[0]].keys():
        return None
    if "categories" in instance.meta.keys():
        return None
    if instance_id == 622:
        return None

    all_categories = set()
    for project in instance:
        all_categories.add(instance.project_meta[project]["category"])

    proportions_per_rule = calculate_proportions_per_rule(instance, profile, COST_SATISFACTION(instance, profile).sat,
                                                          [rule], data, instance_id, all_categories)

    s = 0
    for category in proportions_per_rule[rule].keys():
        diff = proportions_per_rule[rule][category] - proportions_per_rule["Voters"][category]
        s += diff ** 2
    sd = (s / len(proportions_per_rule[rule])) ** 0.5
    if invert:
        return exp(-sd)
    return sd


def gini(data):
    if all((point == 0 for point in data)):
        return 0

    numerator = 0
    denominator = 0
    for i in data:
        denominator += i
        for j in data:
            numerator += abs(i - j)
    denominator *= 2 * len(data)
    return numerator / denominator


def sat_gini(data, rule, instance_id, satisfaction_function, invert=False):
    instance = data[instance_id]["instance"]
    profile = data[instance_id]["profile"]
    winners = data[instance_id][rule]

    satisfactions = calculate_all_satisfactions(instance, profile, winners, satisfaction_function(instance, profile))
    if invert:
        return 1 - gini(satisfactions)
    return gini(satisfactions)


def unhappiness(data, rule, instance_id, invert=False):
    instance = data[instance_id]["instance"]
    profile = data[instance_id]["profile"]
    winners = data[instance_id][rule]
    satisfactions = calculate_all_satisfactions(instance, profile, winners, COST_SATISFACTION(instance, profile))

    total_unhappy = 0
    for satisfaction in satisfactions:
        if satisfaction == 0:
            total_unhappy += 1
    if invert:
        return 1 - total_unhappy / len(satisfactions)
    return total_unhappy / len(satisfactions)


def median_selected_cost(data, rule, instance_id):
    winners = data[instance_id][rule]

    winners_costs = []
    for winner in winners:
        winners_costs.append(winner.cost)
    res = median(winners_costs)

    return res


def write_all_properties(data, overwrite=False):
    instance_ids = list(data.keys())
    rules = list(data[instance_ids[0]].keys())[2:]

    existing_data = dict()

    if not overwrite:
        for rule in rules:
            if exists(f"data/properties_{rule}.csv"):
                existing_data[rule] = dict()
                with open(f"data/properties_{rule}.csv", 'r') as file:
                    reader = csv.DictReader(file, delimiter=';')
                    for row in reader:
                        instance_id = int(row['Instance'])
                        existing_data[rule][instance_id] = row

    properties = {
        "Instance": lambda d, r, i: i,
        "Rule": lambda d, r, i: r,
        "Selected projects": lambda d, r, i: str(d[i][r])[1:-1].replace(' ', ''),
        "Vote count": lambda d, r, i: len(d[i]["profile"]),
        "Project count": lambda d, r, i: len(d[i]["instance"]),
        "Number of winners": n_winners,
        "Similarity to Greedy": cost_similarity_to_cost,
        "Avg cost satisfaction": avg_cost_satisfaction,
        "Category proportionality": lambda d, r, i: category_disproportionality(d, r, i, invert=True),
        "Equality (inverted cost gini)": lambda d, r, i: sat_gini(d, r, i, COST_SATISFACTION, invert=True),
        "Fairness (inverted share gini)": lambda d, r, i: sat_gini(d, r, i, EFFORT_SATISFACTION, invert=True),
        "Happiness (%non-empty-handed)": lambda d, r, i: unhappiness(d, r, i, invert=True),
        "Median selected cost": median_selected_cost
    }

    for rule in rules:
        print(f"Calculating rule {rule}...")
        with open(f"data/properties_{rule}.csv", 'w') as file:
            writer = csv.DictWriter(file, fieldnames=list(properties.keys()), delimiter=';')
            writer.writeheader()
            for instance_id in instance_ids:
                print(f"  instance {instance_id}...")
                row = dict()
                for key, prop in properties.items():
                    try:
                        row[key] = existing_data[rule][instance_id][key]
                    except KeyError:
                        row[key] = prop(data, rule, instance_id)
                writer.writerow(row)


if __name__ == "__main__":
    results = "/Users/Pelle/OneDrive/Documents/MSc/Participatory Budgeting/git/data/results.csv"
    pabulibs = "/Users/Pelle/surfdrive/Shared/AmsterdamBB_clean/pabulib"

    results_data = read_results(results, pabulibs)

    write_all_properties(results_data)

import csv
from os import listdir, mkdir, rename, remove
from warnings import warn


def convert_comma_to_semicolon(file):
    # Open file and create target file
    with open(file, 'r') as source:
        reader = csv.DictReader(source, delimiter=',')
        with open(file[0:-4] + "conv.csv", 'w') as target:
            first_line = True
            for row in reader:
                if first_line:
                    # Translate first line manually
                    writer = csv.DictWriter(target, fieldnames=row.keys(), delimiter=';')
                    keys_string = str(row.keys())
                    keys_string = keys_string[11:-2].replace("'", '').replace(' ', '').replace(',', ';')
                    target.write(keys_string + "\n")
                    first_line = False
                # Translate other lines automatically
                writer.writerow(row)


def parse_string_for_budget(text, budget_pointer):
    # Find the budget pointer in the text
    text = text.lower()
    index = text.find(budget_pointer.lower())
    word_length = len(budget_pointer)

    # Remove all text before budget and clean
    text = text[index + word_length:]
    for char in [' ', '€', '.', ',']:
        text = text.replace(char, '')

    # Read the budget from the beginning of the text
    budget = ""
    for char in text:
        try:
            digit = int(char)
        except ValueError:
            break
        budget += str(digit)
    assert len(budget) != 0, "Empty budget"

    return int(budget)


def parse_file_for_budget(file, budget_pointer):
    # Rename old file, open source file and create target file
    rename_old_file_to = file[:-4] + "_old.csv"
    rename(file, rename_old_file_to)

    with open(rename_old_file_to, 'r', encoding="utf-8-sig") as source:
        reader = csv.DictReader(source, delimiter=';')
        with open(file, 'w') as target:
            first_line = True
            count = 1
            for row in reader:
                count += 1
                if first_line:
                    first_line = False
                    writer = csv.DictWriter(target, fieldnames=row.keys(), delimiter=';')
                    writer.writeheader()

                # Read budget from description of project and write in relevant column
                if row["budget"] == "":
                    text = row["description"]
                    try:
                        budget = parse_string_for_budget(text, budget_pointer)
                    except AssertionError:
                        raise AssertionError(f"Failed to read budget on line {count} of file {file}.")
                    row["budget"] = budget
                    writer.writerow(row)
                else:
                    writer.writerow(row)


def translate_keys(reader, interpret_as):
    # Change fieldnames in reader according to interpret_as
    for i in range(len(reader.fieldnames)):
        key = reader.fieldnames[i]
        if key in [pair[0] for pair in interpret_as]:
            index = [pair[0] for pair in interpret_as].index(key)

            # If the new name already exists as a field name, add an asterix
            if interpret_as[index][1] in reader.fieldnames:
                reader.fieldnames[i] = interpret_as[index][1] + "*"
                warn(f"Key '{interpret_as[index][1]}' already exists."
                     f"Translated '{interpret_as[index][0]}' to '{interpret_as[index][1] + '*'}'")

            reader.fieldnames[i] = interpret_as[index][1]


def sort_dictionary_by(dictionary, sort_list):
    """
    Note that dictionaries are only sortable in Python3.7+
    """
    old_dict = dictionary
    new_dict = dict()

    # Copy entries of old_dict to new_dict in order of sort_list
    already_inserted = []
    for key in sort_list:
        if key in old_dict.keys():
            new_dict[key] = old_dict[key]
            already_inserted.append(key)

    # Copy all entries of old_dict whose keys are not in sort_list
    for key in old_dict.keys():
        if key not in already_inserted:
            new_dict[key] = old_dict[key]

    return new_dict


def sort_meta_as_pabulib(dictionary):
    default_keys = ["description", "country", "unit", "subunit", "instance", "num_projects", "num_votes", "budget",
                    "vote_type", "rule", "date_begin", "date_end", "language", "edition", "district", "comment",
                    "min_length", "max_length", "min_sum_cost", "max_sum_cost", "scoring_fn", "min_points",
                    "max_points", "min_sum_points", "max_sum_points", "default_score"]
    return sort_dictionary_by(dictionary, default_keys)


def sort_projects_as_pabulib(dictionary):
    default_keys = ["project_id", "cost", "name", "category", "target"]
    return sort_dictionary_by(dictionary, default_keys)


def sort_votes_as_pabulib(dictionary):
    default_keys = ["voter_id", "age", "sex", "voting_method", "vote", "points"]
    return sort_dictionary_by(dictionary, default_keys)


def check_compatibility(meta_file, projects_directory, votes_directory):
    so_far_so_good = True

    # Collect site IDs in meta file
    meta_site_ids = set()
    with open(meta_file, 'r') as meta:
        reader = csv.DictReader(meta, delimiter=';')

        count = 1
        for row in reader:
            count += 1
            try:
                site_id = int(row["siteId"])
                meta_site_ids.add(site_id)
            except ValueError:
                so_far_so_good = False
                warn(f"Line {count} of meta file has no integer as site ID.")

    # Collect site IDs in projects file
    projects_site_ids = set()
    for file in listdir(projects_directory):
        try:
            site_id = int(file[0:-4])
            projects_site_ids.add(site_id)
        except ValueError:
            if file[0] != '.':
                so_far_so_good = False
                warn(f"Project file {file} has no site ID as file name.")

    # Collect site IDs in votes file
    votes_site_ids = set()
    for file in listdir(votes_directory):
        try:
            site_id = int(file[0:-4])
            votes_site_ids.add(site_id)
        except ValueError:
            if file[0] != '.':
                so_far_so_good = False
                warn(f"Votes file {file} has no site ID as file name.")

    # Compare the sets of project IDs and print their difference if it exists
    if meta_site_ids == projects_site_ids and projects_site_ids == votes_site_ids:
        return so_far_so_good
    else:
        print("Files do not match.")
        print(f"Missing site IDs in meta:     {(projects_site_ids | votes_site_ids) - meta_site_ids}")
        print(f"Missing site IDs in projects: {(meta_site_ids | votes_site_ids) - projects_site_ids}")
        print(f"Missing site IDs in votes:    {(meta_site_ids | projects_site_ids) - votes_site_ids}")
        return False


def votes_match_projects(projects_file, votes_file):
    projects = set()

    # Fill projects set with all project IDs in projects file
    with open(projects_file, 'r', encoding="utf-8-sig") as file:
        reader = csv.DictReader(file, delimiter=';')

        count = 1
        for row in reader:
            count += 1
            try:
                projects.add(int(row["project_id"]))
            except ValueError:
                raise ValueError(f"Project ID in line {count} of project file {projects_file} "
                                 f"is not an integer.")

    # Check that each entry in the votes file is a vote for an existing project
    with open(votes_file, 'r', encoding="utf-8-sig") as file:
        reader = csv.DictReader(file, delimiter=',')

        count = 1
        for row in reader:
            count += 1
            try:
                vote = int(row["ideaId"])
            except ValueError:
                raise ValueError(f"Vote in line {count} of votes file {votes_file} "
                                 f"is not an integer.")

            assert vote in projects, f"Vote in line {count} of votes file {votes_file} " \
                                     f"is not a project in project file {projects_file}."

        return True


def project_categories_match_meta(meta_file, projects_file, category_pointers):
    so_far_so_good = True
    categories = set()

    # Fill categories set with all categories in projects file
    with open(projects_file, 'r', encoding="utf-8-sig") as file:
        reader = csv.DictReader(file, delimiter=';')

        first_line = True
        count = 1
        for row in reader:
            count += 1
            if first_line:
                first_line = False
                try:
                    site_id = int(row["siteId"])
                except KeyError:
                    raise KeyError(f"File {projects_file} has no key for site ID.")
                except TypeError:
                    raise TypeError(f"Line {count} of file {projects_file} has no integer as site ID.")

            for pointer in category_pointers:
                try:
                    categories.add(row[pointer].replace(',', ''))
                except KeyError:
                    pass

    # Check that each category in the set is in the meta file
    with open(meta_file, 'r', encoding="utf-8-sig") as file:
        reader = csv.DictReader(file, delimiter=';')

        for row in reader:
            if int(row["siteId"]) == site_id:
                for category in categories:
                    if row["categories"] != "":
                        if row["categories"].find(category) == -1:
                            so_far_so_good = False
                            warn(f"Category '{category}' is not in meta data of instance {site_id}")

        return so_far_so_good


def write_meta_data(source_file, target_directory, interpret_as: list[tuple[str, str]] = ()):
    # Create target directory
    try:
        mkdir(target_directory)
    except FileExistsError:
        files = listdir(target_directory)
        if len(files) != 0:
            while True:
                should_remove = input("The target directory already exists and contains files. "
                                      "Do you want to remove the files (Y/n)? ")
                if should_remove == "Y":
                    for file in files:
                        remove(target_directory + '/' + file)
                    break
                elif should_remove == "n":
                    exit()

    # Read site IDs
    site_ids = []
    with open(source_file, 'r') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=';')
        translate_keys(reader, interpret_as)

        count = 1
        for row in reader:
            count += 1
            try:
                site_id = int(row["instance"])
            except ValueError:
                raise ValueError(f"Cannot find site ID in line {count} of meta data.")
            site_ids.append(site_id)

            # For each site ID, create a pabulib file and fill it with all available data
            with open(target_directory + '/' + str(site_id) + ".pb", 'w') as pabulib:
                writer = csv.writer(pabulib, delimiter=';')
                writer.writerow(["META"])
                writer.writerow(["key", "value"])

                row = sort_meta_as_pabulib(row)
                for key in row.keys():
                    value = row[key]
                    if value != "":
                        if key in [pair[0] for pair in interpret_as]:
                            index = [pair[0] for pair in interpret_as].index(key)
                            writer.writerow([interpret_as[index][1], value])
                        else:
                            writer.writerow([key, value])
    return site_ids


def add_projects_to_pabulib(source_directory, target_directory, target_site_id,
                            interpret_as: list[tuple[str, str]] = (), add_if_possible: list[str] = ()):
    # Open projects file and pabulib file
    with open(source_directory + '/' + str(target_site_id) + ".csv", 'r', encoding="utf-8-sig") as csv_file:
        reader = csv.DictReader(csv_file, delimiter=';')
        translate_keys(reader, interpret_as)

        with open(target_directory + '/' + str(target_site_id) + ".pb", 'a') as pabulib:
            writer = csv.writer(pabulib, delimiter=';')
            writer.writerow(["PROJECTS"])
            keys_to_write = ["project_id", "cost", "name"]

            count = 1
            key_possible_to_add = [False for _ in add_if_possible]
            for row in reader:
                count += 1
                values_to_write = []

                # Check whether the project file contains category data and write header
                if count == 2:
                    for i in range(len(add_if_possible)):
                        key = add_if_possible[i]
                        try:
                            if row[key] != "":
                                keys_to_write.append(key)
                                key_possible_to_add[i] = True
                        except KeyError:
                            pass
                    writer.writerow(keys_to_write)

                # Read relevant data
                try:
                    site_id = int(row["siteId"])
                except KeyError:
                    raise KeyError(f"File {csv_file.name} has no key for site ID.")
                except TypeError:
                    raise TypeError(f"Line {count} of file {csv_file.name} has no integer as site ID.")
                assert site_id == target_site_id, f"Line {count} of file {csv_file.name} has site ID " \
                                                  f"{site_id} instead of {target_site_id}."

                try:
                    project_id = int(row["project_id"])
                except ValueError:
                    raise ValueError(f"Project ID in line {count} of project file {target_site_id} "
                                     f"is '{row['project_id']}' instead of an integer.")
                values_to_write.append(project_id)

                try:
                    cost = int(row["cost"].replace('.', '').replace(',', ''))
                except ValueError:
                    raise ValueError(f"Budget in line {count} of project file {target_site_id} "
                                     f"is '{row['budget']}' instead of an integer.")
                if cost == 0:
                    warn(f"Budget of idea {project_id} in instance {site_id} is zero.")
                values_to_write.append(cost)

                name = row["name"].replace(';', '')
                values_to_write.append(name)

                for i in range(len(add_if_possible)):
                    if key_possible_to_add[i]:
                        value = row[add_if_possible[i]].replace(',', '')
                        values_to_write.append(value)

                # Write to pabulib file
                writer.writerow(values_to_write)


def add_votes_to_pabulib(source_directory, target_directory, target_site_id):
    # Open votes file
    with open(source_directory + '/' + str(target_site_id) + ".csv", 'r') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=',')

        # Read relevant data into directory votes
        votes = dict()
        count = 1
        for row in reader:
            count += 1
            try:
                site_id = int(row["siteId"])
            except ValueError:
                raise ValueError(f"Line {count} of votes file {target_site_id} does not contain an integer site ID.")
            assert site_id == target_site_id, f"Line {count} of votes file {target_site_id} contains site ID " \
                                              f"{site_id} instead of {target_site_id}."

            voter_id = int(row["userId"])
            vote = int(row["ideaId"])
            if voter_id not in votes:
                votes[voter_id] = {'voter_id': voter_id, 'ballot': []}
            votes[voter_id]['ballot'].append(vote)

        # Write to pabulib file
        with open(target_directory + '/' + str(target_site_id) + ".pb", 'a') as pabulib:
            writer = csv.writer(pabulib, delimiter=';')
            writer.writerow(["VOTES"])
            writer.writerow(["voter_id", "vote"])
            for vote in votes:
                voter_id = votes[vote]['voter_id']
                ballot = votes[vote]['ballot']
                writer.writerow([voter_id, str(ballot)[1:-1].replace(' ', '')])


def add_counts_to_pabulib(pabulib_file):
    # Load file under different name
    old_file = pabulib_file[:-3] + "_old.pb"
    rename(pabulib_file, old_file)
    with open(old_file, 'r', encoding="utf-8-sig") as source_file:
        reader = csv.reader(source_file, delimiter=';')

        # Iterate over the rows and count lines in the relevant sections
        current_section = None
        project_count = 0
        vote_count = 0
        for row in reader:
            if str(row[0]).strip().lower() in ["meta", "projects", "votes"]:
                current_section = str(row[0]).strip().lower()
                next(reader)
            elif current_section == "projects":
                project_count += 1
            elif current_section == "votes":
                vote_count += 1

    # Load file again and create new file
    with open(old_file, 'r', encoding="utf-8-sig") as source_file:
        reader = csv.reader(source_file, delimiter=';')
        with open(pabulib_file, 'w') as target_file:
            writer = csv.writer(target_file, delimiter=';')

            # Copy all lines
            for row in reader:
                writer.writerow(row)

                # Add num_projects and num_votes after instance ID
                if row[0].strip().lower() == "instance":
                    writer.writerow(["num_projects", project_count])
                    writer.writerow(["num_votes", vote_count])

    remove(old_file)


if __name__ == "__main__":

    source_file_meta = "/Users/Pelle/surfdrive/Shared/AmsterdamBB_clean/meta_data.csv"
    source_directory_projects = "/Users/Pelle/surfdrive/Shared/AmsterdamBB_clean/ideas"
    source_directory_votes = "/Users/Pelle/surfdrive/Shared/AmsterdamBB_clean/votes"
    target_directory_pabulib = "/Users/Pelle/surfdrive/Shared/AmsterdamBB_clean/pabulib"

    print("Checking compatibility...")
    if check_compatibility(source_file_meta, source_directory_projects, source_directory_votes):
        print("Writing meta data...")
        sites = write_meta_data(source_file_meta, target_directory_pabulib, interpret_as=[("siteId", "instance")])
        counter = 0
        for site in sites:
            counter += 1
            project_file_path = source_directory_projects + '/' + str(site) + ".csv"
            votes_file_path = source_directory_votes + '/' + str(site) + ".csv"
            target_file_path = target_directory_pabulib + '/' + str(site) + ".pb"
            print(f"Writing instance {counter}/{len(sites)}...")
            if votes_match_projects(project_file_path, votes_file_path) and \
                    project_categories_match_meta(source_file_meta, project_file_path, ["category", "extraData.theme"]):
                add_projects_to_pabulib(source_directory_projects, target_directory_pabulib, site,
                                        interpret_as=[("project_id", "project_id"), ("budget", "cost"),
                                                      ("title", "name"), ("extraData.theme", "category")],
                                        add_if_possible=["category", "neighbourhood"])
                add_votes_to_pabulib(source_directory_votes, target_directory_pabulib, site)
                add_counts_to_pabulib(target_file_path)

import csv
import matplotlib.pyplot as plt
import scipy.stats as stats
from os import listdir
from re import compile
from numpy import mean, std, arange, floor, log10, median
from copy import deepcopy


def read_properties_files(directory, rules=None, property_names=None, min_votes=0, min_projects=0):
    properties = dict()

    if rules is None:
        rules = []
        files = listdir(directory)
        pattern = compile("properties_.+\.csv")
        for file in files:
            if pattern.match(file):
                rules.append(file[11:-4])

    if property_names is None:
        with open(f"{directory}/properties_{rules[0]}.csv", 'r') as file:
            reader = csv.reader(file, delimiter=';')
            property_names = next(reader)[3:]

    for key in property_names:
        properties[key] = dict()
        for rule in rules:
            properties[key][rule] = dict()
    for rule in rules:
        with open(f"{directory}/properties_{rule}.csv", 'r') as file:
            reader = csv.DictReader(file, delimiter=';')
            for row in reader:
                instance_id = int(row["Instance"])
                n_votes = int(row["Vote count"])
                n_projects = int(row["Project count"])
                if n_votes >= min_votes and n_projects >= min_projects:
                    for key in property_names:
                        if row[key] in ["None", "nan", '']:
                            properties[key][rule][instance_id] = None
                        else:
                            properties[key][rule][instance_id] = float(row[key])
    return properties


def remove_uncategorized_instances(properties):
    count = 0
    rule = list(properties["Category proportionality"].keys())[0]
    to_delete = []
    for instance_id, value in properties["Category proportionality"][rule].items():
        if value is not None:
            count += 1
        else:
            to_delete.append(instance_id)

    for rule in properties["Category proportionality"].keys():
        for instance_id in to_delete:
            del properties["Category proportionality"][rule][instance_id]

    return count


def round_nicely(f, n_digits, max_decimals=None):
    significant_digit = -int(floor(log10(abs(f))))
    last_digit = significant_digit + n_digits - 1
    if last_digit <= 0:
        return str(round(f))
    else:
        assert last_digit < 20
        rounded = f"{round(float(f), last_digit):.20f}"
        if max_decimals is not None:
            break_at = min(last_digit, max_decimals) - 20
        else:
            break_at = last_digit - 20
        return rounded[:break_at]


def order_of_magnitude(k):
    return int(floor(log10(k)) + 1)


def plot_everything(properties, rules=None, rule_names=None, reference_rule=None, name=None,
                    plot_properties=None, property_names=None, plot_instances=(), arbitrary_units=True):
    properties = deepcopy(properties)

    if rules is None:
        prop = list(properties.keys())[0]
        rules = list(properties[prop].keys())

    if rule_names is None:
        rule_names = rules

    if reference_rule is not None:
        rules.remove(reference_rule)
        rules = [reference_rule] + rules

    prop = list(properties.keys())[0]
    rule = rules[0]
    n_instances = len(properties[prop][rule])

    if "Category proportionality" in properties.keys() and "Category proportionality" in plot_properties:
        n_cat_inst = remove_uncategorized_instances(properties)
        i = plot_properties.index("Category proportionality")
        property_names[i] += f"\n({n_cat_inst} instances)"

    to_plot = dict()
    for property_name in plot_properties:

        to_plot[property_name] = {
            'avgs': [],
            'errors': [],
            'p-values': []
        }
        for rule in rules:
            values = list(properties[property_name][rule].values())
            avg = mean(values)
            error = std(values) / (len(values) ** 0.5)
            if rule == rules[0]:
                reference_values = values
                p_value = None
            else:
                # noinspection PyUnboundLocalVariable
                p_value = float(stats.ttest_rel(reference_values, values)[1])
            to_plot[property_name]['avgs'].append(avg)
            to_plot[property_name]['errors'].append(error)
            to_plot[property_name]['p-values'].append(p_value)

    to_plot_normalized = deepcopy(to_plot)
    if arbitrary_units:
        reference_values = dict()
        for key, value in to_plot_normalized.items():
            reference_value = value["avgs"][0]
            reference_values[key] = reference_value
            for i in range(len(value["avgs"])):
                value["avgs"][i] /= reference_value
                value["errors"][i] /= reference_value

    y = arange(len(to_plot_normalized), 0, -1)
    height = 1 / (len(rules) + 1)
    multiplier = 0

    fig, ax = plt.subplots()

    instance_plots = dict()
    for instance_id in plot_instances:
        instance_plots[instance_id] = [[], []]

    for i in range(len(rules)):
        offset = height * multiplier
        x = [to_plot_normalized[prop]["avgs"][i] for prop in to_plot_normalized.keys()]
        x_labels = [to_plot[prop]["avgs"][i] for prop in to_plot.keys()]
        errors = [to_plot_normalized[prop]["errors"][i] for prop in to_plot_normalized.keys()]
        p_values = [to_plot_normalized[prop]["p-values"][i] for prop in to_plot_normalized.keys()]
        if len(plot_instances) == 0:
            ax.barh(y + offset, x, height, label=rule_names[i], xerr=errors, align='center', capsize=3)
        else:
            ax.barh(y + offset, x, height, label=rule_names[i])
        for j in range(len(x)):
            ax.text(0.02, y[j] + offset, round_nicely(x_labels[j], 3), ha='left', va='center', fontsize=9)
            if i != 0 and len(plot_instances) == 0:
                if p_values[j] <= 0.05:
                    ax.text(x[j] - errors[j] - 0.02, y[j] + offset, fr"$p < 10^{{{order_of_magnitude(p_values[j])}}}$",
                            ha='right', va='center', fontsize=8)
                else:
                    ax.text(x[j] - errors[j] - 0.02, y[j] + offset, fr"$p < 10^{{{order_of_magnitude(p_values[j])}}}$",
                            ha='right', va='center', fontsize=8, color='w')

        for instance_id in plot_instances:
            y_instance = list(y + offset)
            x_instance = []
            index = 0
            for prop in to_plot.keys():
                try:
                    value_to_plot = properties[prop][rules[i]][instance_id]
                    if arbitrary_units:
                        value_to_plot /= reference_values[prop]
                    x_instance.append(value_to_plot)
                    index += 1
                except KeyError:
                    y_instance.pop(index)
            instance_plots[instance_id][0] += x_instance
            instance_plots[instance_id][1] += y_instance
        multiplier -= 1

    instance_scatters = []
    for instance_id, (xs, ys) in instance_plots.items():
        instance_scatters.append(ax.scatter(xs, ys))

    if name is None:
        ax.set_title(f"Properties of rules ({n_instances} instances)")
    else:
        ax.set_title(name + f"\n(based on {n_instances} instances)")
    if property_names is None:
        y_labels = list(to_plot_normalized.keys())
        for i in range(len(y_labels)):
            y_labels[i] = y_labels[i].replace(' ', '\n')
    else:
        y_labels = property_names
    ax.set_yticks(y + (len(rules) - 5) / 2 * height, y_labels, fontsize=8)
    plt.tick_params(axis='x', which='both', bottom=False, labelbottom=False)
    if len(plot_instances) > 0:
        instance_labels = list(instance_plots.keys())
        for i in range(len(instance_labels)):
            instance_labels[i] = f"Instance {instance_labels[i]}"
        legend_instances = plt.legend(instance_scatters, instance_labels, loc='upper right')
        plt.gca().add_artist(legend_instances)
    ax.legend(loc='lower center', bbox_to_anchor=(0.5, -0.1), ncol=3)
    plt.tight_layout()
    if name is None:
        plt.savefig(f"plots/rule properties/all.pdf")
    else:
        plt.savefig(f"plots/rule properties/all_{name}.pdf")
    plt.close()


def select_quadrants(data):
    rule = list(data["Vote count"].keys())[0]
    instance_ids = list(data["Vote count"][rule].keys())

    n_votes_per_instance = dict()
    n_projects_per_instance = dict()
    for instance_id in instance_ids:
        n_votes = data["Vote count"][rule][instance_id]
        n_projects = data["Project count"][rule][instance_id]
        n_votes_per_instance[instance_id] = n_votes
        n_projects_per_instance[instance_id] = n_projects
    median_votes = median(list(n_votes_per_instance.values()))
    median_projects = median(list(n_projects_per_instance.values()))
    empty_data_struc = dict()
    for prop in data.keys():
        empty_data_struc[prop] = dict()
        for rule in data[prop].keys():
            empty_data_struc[prop][rule] = dict()
    quadrants = {
        "few votes, few projects": deepcopy(empty_data_struc),
        "few votes, many projects": deepcopy(empty_data_struc),
        "many votes, few projects": deepcopy(empty_data_struc),
        "many votes, many projects": deepcopy(empty_data_struc)
    }
    for instance_id in instance_ids:
        if n_votes_per_instance[instance_id] < median_votes:
            if n_projects_per_instance[instance_id] <= median_projects:
                for prop in data.keys():
                    for rule in data[prop].keys():
                        quadrants["few votes, few projects"][prop][rule][instance_id] = data[prop][rule][instance_id]
            else:
                for prop in data.keys():
                    for rule in data[prop].keys():
                        quadrants["few votes, many projects"][prop][rule][instance_id] = data[prop][rule][instance_id]
        else:
            if n_projects_per_instance[instance_id] <= median_projects:
                for prop in data.keys():
                    for rule in data[prop].keys():
                        quadrants["many votes, few projects"][prop][rule][instance_id] = data[prop][rule][instance_id]
            else:
                for prop in data.keys():
                    for rule in data[prop].keys():
                        quadrants["many votes, many projects"][prop][rule][instance_id] = data[prop][rule][instance_id]
    return quadrants


def best_improvement(dict_of_coordinates):
    maximum = None
    argmax = None
    for key, coordinates in dict_of_coordinates.items():
        improvement = min(list(coordinates.values()))
        if maximum is None or improvement > maximum:
            maximum = improvement
            argmax = key
    return argmax


def worst_improvement(dict_of_coordinates):
    minimum = None
    argmin = None
    for key, coordinates in dict_of_coordinates.items():
        improvement = max(list(coordinates.values()))
        if minimum is None or improvement < minimum:
            minimum = improvement
            argmin = key
    return argmin


def median_improvement(dict_of_coordinates):
    improvements = dict()
    for key, coordinates in dict_of_coordinates.items():
        improvement = mean(list(coordinates.values()))
        improvements[key] = improvement
    median_size = median(list(improvements.values()))

    minimum_to_median = None
    argmin = None
    for key, improvement in improvements.items():
        dist_to_median = abs(improvement - median_size)
        if minimum_to_median is None or dist_to_median < minimum_to_median:
            minimum_to_median = dist_to_median
            argmin = key

    return argmin


def find_examples(properties, properties_to_read=None, reference_rule="Greedy",
                  target_rule="Completed iterated MES"):
    rules = [reference_rule, target_rule]
    properties = deepcopy(properties)

    if properties_to_read is None:
        properties_to_read = list(properties.keys())

    instance_ids = list(properties[properties_to_read[0]][reference_rule].keys())

    ids_to_remove = set()
    for instance_id in instance_ids:
        for key in properties_to_read:
            for rule in rules:
                if properties[key][rule][instance_id] is None:
                    ids_to_remove.add(instance_id)

    for instance_id in ids_to_remove:
        instance_ids.remove(instance_id)
        for key in properties_to_read:
            for rule in rules:
                del properties[key][rule][instance_id]

    changes_per_instance = dict()
    for instance_id in instance_ids:
        changes = dict()
        for key in properties_to_read:
            avg_target = properties[key][target_rule][instance_id]
            avg_reference = properties[key][reference_rule][instance_id]
            changes[key] = avg_target / avg_reference - 1
        changes_per_instance[instance_id] = changes

    print(changes_per_instance)

    worst = worst_improvement(changes_per_instance)
    med = median_improvement(changes_per_instance)
    best = best_improvement(changes_per_instance)

    print(f"Worst changes in instance {worst}: {changes_per_instance[worst]}")
    print(f"Median changes in instance {med}: {changes_per_instance[med]}")
    print(f"Best changes in instance {best}: {changes_per_instance[best]}")

    return worst, med, best


if __name__ == "__main__":
    data_dir = "/Users/Pelle/OneDrive/Documents/MSc/Participatory Budgeting/git/data"

    properties_data = read_properties_files(data_dir, min_votes=100, min_projects=10)

    ex = find_examples(properties_data,
                       properties_to_read=["Equality (inverted cost gini)", "Category proportionality"])

    plot_everything(properties_data, rules=["Greedy", "Completed MES", "Completed iterated MES"],
                    rule_names=["GreedCost", r"MES$^{+}$", r"MES$^{{*}{+}}$"],
                    name=r"Fairness properties of GreedCost, MES$^{+}$ and MES$^{{*}{+}}$ ",
                    plot_instances=ex,
                    plot_properties=["Avg cost satisfaction", "Equality (inverted cost gini)",
                                     "Fairness (inverted share gini)", "Happiness (%non-empty-handed)"],
                    property_names=["Avg cost\nsatisfaction", "Equality\n(inverted\ncost Gini)",
                                    "Fairness\n(inverted\neffort Gini)", "Happiness\n(%non-empty-\nhanded)"],
                    arbitrary_units=False)
    plot_everything(properties_data, rules=["Greedy", "Completed MES", "Completed iterated MES"],
                    rule_names=["GreedCost", r"MES$^{+}$", r"MES$^{{*}{+}}$"],
                    name=r"General properties of GreedCost, MES$^{+}$ and MES$^{{*}{+}}$ ",
                    plot_instances=ex,
                    plot_properties=["Similarity to Greedy", "Number of winners",
                                     "Median selected cost", "Category proportionality"],
                    property_names=["Similarity to\nGreedCost", "Number of\nwinners",
                                    "Median\nselected cost", "Category\nproportionality"])

    plot_everything(properties_data, rules=["Greedy", "Completed MES", "Completed iterated MES"],
                    rule_names=["GreedCost", r"MES$^{+}$", r"MES$^{{*}{+}}$"],
                    name=r"General properties of GreedCost, MES$^{+}$ and MES$^{{*}{+}}$",
                    plot_properties=["Similarity to Greedy", "Number of winners",
                                     "Median selected cost", "Category proportionality"],
                    property_names=["Similarity to\nGreedCost", "Number of\nwinners",
                                    "Median\nselected cost", "Category\nproportionality"])
    plot_everything(properties_data, rules=["Greedy", "Completed MES", "Completed iterated MES"],
                    rule_names=["GreedCost", r"MES$^{+}$", r"MES$^{{*}{+}}$"],
                    name=r"Fairness properties of GreedCost, MES$^{+}$ and MES$^{{*}{+}}$",
                    plot_properties=["Avg cost satisfaction", "Equality (inverted cost gini)",
                                     "Fairness (inverted share gini)", "Happiness (%non-empty-handed)"],
                    property_names=["Avg cost\nsatisfaction", "Equality\n(inverted\ncost Gini)",
                                    "Fairness\n(inverted\neffort Gini)", "Happiness\n(%non-empty-\nhanded)"],
                    arbitrary_units=False)

    # for label, quadrant in select_quadrants(properties_data).items():
    #     plot_everything(properties_data, rules=["Greedy", "Completed MES", "Completed iterated MES"],
    #                     rule_names=["GreedCost", r"MES$^{+}$", r"MES$^{{*}{+}}$"],
    #                     name=label + "; fairness properties",
    #                     plot_properties=["Avg cost satisfaction", "Equality (inverted cost gini)",
    #                                      "Fairness (inverted share gini)", "Happiness (%non-empty-handed)"],
    #                     property_names=["Avg cost\nsatisfaction", "Equality\n(inverted cost Gini)",
    #                                     "Fairness\n(inverted effort Gini)", "Happiness\n(%non-empty-handed)"])
    #     plot_everything(properties_data, rules=["Greedy", "Completed MES", "Completed iterated MES"],
    #                     rule_names=["GreedCost", r"MES$^{+}$", r"MES$^{{*}{+}}$"],
    #                     name=label + "; other properties",
    #                     plot_properties=["Number of winners", "Similarity to Greedy", "Median selected cost"],
    #                     property_names=["Number of\nwinners", "Similarity to\nGreedCost", "Median\nselected cost"])

import matplotlib.pyplot as plt
from os import listdir
from sklearn.cluster import KMeans
from numpy import median, std, corrcoef

from pbvoting.instance.pbinstance import total_cost
from pbvoting.instance.pabulib import parse_pabulib


def read_instances(pabulib_directory, ignore=(), min_votes=0, min_projects=0):
    instance_profile_pairs = []
    for pabulib_file in listdir(pabulib_directory):
        if pabulib_file[-3:] == ".pb":
            if int(pabulib_file[-6:-3]) not in ignore:
                instance, profile = parse_pabulib(pabulib_directory + '/' + pabulib_file)
                if int(instance.meta["num_votes"]) >= min_votes:
                    if int(instance.meta["num_projects"]) >= min_projects:
                        instance_profile_pairs.append((instance, profile))
    return instance_profile_pairs


def plot_vote_and_project_count_clustered(instance_profile_pairs, n_clusters):
    instances = []
    max_vote_count = 0
    max_project_count = 0
    for instance, profile in instance_profile_pairs:
        vote_count = int(instance.meta["num_votes"])
        project_count = int(instance.meta["num_projects"])
        instances.append((vote_count, project_count))

        if max_vote_count < vote_count:
            max_vote_count = vote_count
        if max_project_count < project_count:
            max_project_count = project_count

    normalized_instances = []
    for vote_count, project_count in instances:
        normalized_instances.append((vote_count / max_vote_count, project_count / max_project_count))

    kmeans = KMeans(n_clusters=n_clusters, n_init='auto')
    kmeans.fit(normalized_instances)
    labels = kmeans.labels_

    width = max_vote_count * 1.2
    height = max_project_count * 1.1

    clustered_vote_counts = [[] for _ in range(n_clusters)]
    clustered_project_counts = [[] for _ in range(n_clusters)]
    for i in range(len(instances)):
        # noinspection PyTypeChecker
        clustered_vote_counts[labels[i]].append(instances[i][0])
        # noinspection PyTypeChecker
        clustered_project_counts[labels[i]].append(instances[i][1])

    for i in range(n_clusters):
        plt.scatter(clustered_vote_counts[i], clustered_project_counts[i])

    plt.xlim(0, width)
    plt.ylim(0, height)
    plt.xlabel("Vote count")
    plt.ylabel("Project count")
    plt.title(f"Instance sizes in {n_clusters} clusters")
    plt.tight_layout()
    plt.savefig(f"plots/instance properties/scatter_size_clustered_{n_clusters}.pdf")
    plt.close()


def plot_vote_and_project_count(instance_profile_pairs, group_by="district"):
    instances = []
    group_labels = set()
    max_vote_count = 0
    max_project_count = 0
    for instance, profile in instance_profile_pairs:
        instance_dict = dict()
        vote_count = int(instance.meta["num_votes"])
        instance_dict["vote_count"] = vote_count
        project_count = int(instance.meta["num_projects"])
        instance_dict["project_count"] = project_count
        year = int(instance.meta["year"])
        instance_dict["year"] = year

        group_label = instance.meta[group_by]
        instance_dict["group_label"] = group_label
        group_labels.add(group_label)
        instances.append(instance_dict)

        if max_vote_count < vote_count:
            max_vote_count = vote_count
        if max_project_count < project_count:
            max_project_count = project_count

    width = max_vote_count * 1.2
    height = max_project_count * 1.1

    grouped_instances = dict()
    for label in group_labels:
        grouped_instances[label] = []
        for instance in instances:
            if instance["group_label"] == label:
                grouped_instances[label].append(instance)

    for label, group in grouped_instances.items():
        vote_counts = [instance["vote_count"] for instance in group]
        project_counts = [instance["project_count"] for instance in group]
        years = [instance["year"] for instance in group]
        plt.scatter(vote_counts, project_counts, label=label)
        for i in range(len(vote_counts)):
            plt.text(vote_counts[i] + width / 100, project_counts[i], years[i], fontsize=5)

    median_vote_count = median([i["vote_count"] for i in instances])
    median_project_count = median([i["project_count"] for i in instances])
    plt.plot([0, width], [median_project_count] * 2, color='gray', linestyle='--', linewidth='0.5')
    plt.text(width * 0.99, median_project_count,
             "Median project count: " + str(int(median_project_count)),
             verticalalignment="bottom", horizontalalignment="right", fontsize=7, color='gray')
    plt.plot([median_vote_count] * 2, [0, height], color='gray', linestyle='--', linewidth='0.5')
    plt.text(median_vote_count, height * 0.99,
             "Median vote count: " + str(int(median_vote_count)),
             verticalalignment="top", horizontalalignment="right", fontsize=7, color='gray', rotation=90)

    plt.xlim(0, width)
    plt.ylim(0, height)
    plt.xlabel("Vote count")
    plt.ylabel("Project count")
    plt.title(f"Sizes of {len(instances)} Participatory Budgeting instances in Amsterdam")
    plt.legend(loc='upper right')
    plt.tight_layout()
    plt.savefig(f"plots/instance properties/scatter_size.pdf")
    plt.close()


def calculate_budgets(instance_profile_pairs):
    res = []
    for instance, profile in instance_profile_pairs:
        res.append(instance.budget_limit)
    return res


def calculate_money_scarcities(instance_profile_pairs):
    res = []
    for instance, profile in instance_profile_pairs:
        budget = instance.budget_limit
        total_project_cost = total_cost(instance)
        scarcity = total_project_cost / budget
        res.append(scarcity)
    return res


def calculate_vote_counts(instance_profile_pairs):
    res = []
    for instance, profile in instance_profile_pairs:
        res.append(len(profile))
    return res


def calculate_project_counts(instance_profile_pairs):
    res = []
    for instance, profile in instance_profile_pairs:
        res.append(len(instance))
    return res


def calculate_avg_project_cost_normalized(instance_profile_pairs):
    res = []
    for instance, profile in instance_profile_pairs:
        avg_cost = 0
        for project in instance:
            avg_cost += project.cost
        avg_cost /= len(instance) * instance.budget_limit
        res.append(avg_cost)
    return res


def calculate_avg_project_cost(instance_profile_pairs):
    res = []
    for instance, profile in instance_profile_pairs:
        avg_cost = 0
        for project in instance:
            avg_cost += project.cost
        avg_cost /= len(instance)
        res.append(avg_cost)
    return res


def calculate_avg_approved_cost(instance_profile_pairs):
    res = []
    for instance, profile in instance_profile_pairs:
        avg_approved_cost = 0
        approvals = 0
        for ballot in profile:
            for project in ballot:
                approvals += 1
                avg_approved_cost += project.cost
        avg_approved_cost /= approvals
        res.append(avg_approved_cost)
    return res


def calculate_project_cost_spread_normalized(instance_profile_pairs):
    res = []
    for instance, profile in instance_profile_pairs:
        budget = instance.budget_limit
        costs = []
        for project in instance:
            costs.append(project.cost / budget)
        spread = std(costs)
        res.append(spread)
    return res


def calculate_ballot_size(instance_profile_pairs, size_func):
    res = []
    for instance, profile in instance_profile_pairs:
        avg_size = 0
        for ballot in profile:
            avg_size += size_func(ballot) / size_func(instance)
        avg_size /= len(profile)
        res.append(avg_size)
    return res


def plot_everything(instance_profile_pairs):
    properties = {
        "Budget limit\n": calculate_budgets(instance_profile_pairs),
        "Funding scarcity\n": calculate_money_scarcities(instance_profile_pairs),
        "vote count": calculate_vote_counts(instance_profile_pairs),
        "project count": calculate_project_counts(instance_profile_pairs),
        "Average project cost\n(normalized to the budget limit)": calculate_avg_project_cost_normalized(instance_profile_pairs),
        "average project cost": calculate_avg_project_cost(instance_profile_pairs),
        "spread of project costs (normalised to budget)": calculate_project_cost_spread_normalized(
            instance_profile_pairs),
        "average approved project cost": calculate_avg_approved_cost(instance_profile_pairs),
        "relative ballot size (cardinality)": calculate_ballot_size(instance_profile_pairs, len),
        "Total ballot cost\n(normalised to the budget limit)": calculate_ballot_size(instance_profile_pairs, total_cost)
    }

    for key, value in properties.items():
        print(key)
        print(min(value))
        print(median(value))
        print(max(value))
        plt.figure().set_figheight(2.2)
        plt.boxplot(value, widths=0.7, vert=False)
        plt.title(key, fontsize=18)
        plt.tick_params(left=False, labelleft=False, labelsize=16)
        plt.tight_layout()
        plt.savefig(f"plots/instance properties/boxplots/boxplot_{key}.pdf")
        plt.close()

    for key1, value1 in properties.items():
        for key2, value2 in properties.items():
            if key1 > key2:
                plt.scatter(value1, value2)
                min_max = min([max(value1), max(value2)])
                plt.plot([0, min_max], [0, min_max])
                correlation = round(abs(corrcoef(value1, value2)[0, 1]), 2)
                plt.title(f"correlation {correlation}")
                plt.xlabel(key1)
                plt.ylabel(key2)
                plt.tight_layout()
                plt.savefig(f"plots/instance properties/scatter/{correlation}_scatter_{key1}_{key2}.pdf")
                plt.close()


if __name__ == "__main__":
    pabulibs = "/Users/Pelle/surfdrive/Shared/AmsterdamBB_clean/pabulib"
    data = read_instances(pabulibs, min_votes=100, min_projects=10, ignore=[588])

    plot_vote_and_project_count_clustered(data, 2)
    plot_vote_and_project_count_clustered(data, 3)
    plot_vote_and_project_count_clustered(data, 4)
    plot_vote_and_project_count_clustered(data, 5)
    plot_vote_and_project_count_clustered(data, 6)

    plot_vote_and_project_count(data)

    plot_everything(data)

from numpy import arange
import matplotlib.pyplot as plt

from pbvoting.instance.satisfaction import COST_SATISFACTION
from pbvoting.instance.pbinstance import total_cost

from plot_rule_properties import read_properties_files
from calculate_rule_properties import read_results
from calculate_rule_properties import calculate_all_satisfactions
from calculate_rule_properties import calculate_proportions_per_rule


def winners_in_instance(res_data, instance_id, rule):
    return res_data[instance_id][rule]


def isolate_instance_properties(prop_data, instance_id, rule):
    res = dict()
    for prop in prop_data.keys():
        res[prop] = prop_data[prop][rule][instance_id]
    return res


def print_winners(res_data, instance_id, reference_rule, reference_label, target_rule, target_label):
    instance = res_data[instance_id]["instance"]

    winners_reference = winners_in_instance(res_data, instance_id, reference_rule)
    winners_target = winners_in_instance(res_data, instance_id, target_rule)

    selected_both = winners_reference.intersection(winners_target)
    selected_reference = winners_reference - winners_target
    selected_target = winners_target - winners_reference

    with open(f"plots/examples/{instance_id}_winners_{reference_rule}_{target_rule}.txt", 'w') as file:
        file.write(f"Instance {instance_id}:\n")

        file.write(f"  Selected by both rules (€{int(total_cost(selected_both))}):\n")
        for project in selected_both:
            try:
                file.write(f"    {instance.project_meta[project]['name']} "
                           f"({instance.project_meta[project]['category']}, €{int(project.cost)})\n")
            except KeyError:
                file.write(
                    f"    {instance.project_meta[project]['name']} (€{int(project.cost)})\n")
        file.write("\n")

        file.write(f"  Selected by {reference_label} (€{int(total_cost(selected_reference))}):\n")
        for project in selected_reference:
            try:
                file.write(f"    {instance.project_meta[project]['name']} "
                           f"({instance.project_meta[project]['category']}, €{int(project.cost)})\n")
            except KeyError:
                file.write(
                    f"    {instance.project_meta[project]['name']} (€{int(project.cost)})\n")
        file.write("\n")

        file.write(f"  Selected by {target_label} (€{int(total_cost(selected_target))}):\n")
        for project in selected_target:
            try:
                file.write(f"    {instance.project_meta[project]['name']} "
                           f"({instance.project_meta[project]['category']}, €{int(project.cost)})\n")
            except KeyError:
                file.write(
                    f"    {instance.project_meta[project]['name']} (€{int(project.cost)})\n")


def plot_category_proportionality(res_data, instance_id, reference_rule, reference_label, target_rule, target_label):
    instance = res_data[instance_id]["instance"]
    profile = res_data[instance_id]["profile"]
    rules = [reference_rule, target_rule]

    all_categories = set()
    for project in instance:
        all_categories.add(instance.project_meta[project]["category"])

    proportions_per_rule = calculate_proportions_per_rule(instance, profile, COST_SATISFACTION(instance, profile).sat,
                                                          rules, res_data, instance_id, all_categories)

    all_categories = list(all_categories)
    all_categories.sort(key=lambda cat: proportions_per_rule["Voters"][cat], reverse=True)

    x = arange(len(all_categories))  # the label locations
    width = 1 / (len(proportions_per_rule) + 1)  # the width of the bars
    multiplier = 0

    fig, ax = plt.subplots()
    fig.set_figheight(4 + max([len(label) for label in all_categories]) / 20)

    for rule, proportions in proportions_per_rule.items():
        offset = width * multiplier
        y = [proportions[cat] for cat in all_categories]
        if rule == "Voters":
            label = "Voters"
            color = 'g'
        elif rule == reference_rule:
            label = reference_label
            color = 'r'
        else:
            label = target_label
            color = 'b'
        ax.bar(x + offset, y, width, label=label, color=color)
        multiplier += 1

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel(f'Proportion of budget', fontsize=12)
    ax.set_title(f'Category proportionality of PB instance {instance_id}:\n{reference_label} versus {target_label}',
                 fontsize=16)
    ax.set_xticks(x + (len(proportions_per_rule)-1)/2 * width, all_categories, rotation=45, ha="right", fontsize=12)
    ax.legend(fontsize=14)
    plt.tight_layout()
    plt.savefig(f"plots/examples/{instance_id}_prop_{reference_rule}_{target_rule}.pdf")
    plt.close()


def plot_satisfaction_distribution(res_data, instance_id, reference_rule, reference_label, target_rule, target_label):
    instance = res_data[instance_id]["instance"]
    profile = res_data[instance_id]["profile"]

    winners_reference = winners_in_instance(res_data, instance_id, reference_rule)
    winners_target = winners_in_instance(res_data, instance_id, target_rule)
    selected_both = winners_reference.intersection(winners_target)

    satisfactions_both = sorted(calculate_all_satisfactions(instance, profile, selected_both,
                                                            COST_SATISFACTION(instance, profile), normalize=True))
    satisfactions_reference = sorted(calculate_all_satisfactions(instance, profile, winners_reference,
                                                                 COST_SATISFACTION(instance, profile), normalize=True))
    satisfactions_target = sorted(calculate_all_satisfactions(instance, profile, winners_target,
                                                              COST_SATISFACTION(instance, profile), normalize=True))

    plt.plot(range(len(satisfactions_both)), satisfactions_both, label="Both", color='g')
    plt.plot(range(len(satisfactions_reference)), satisfactions_reference, label=reference_label, color='r')
    plt.plot(range(len(satisfactions_target)), satisfactions_target, label=target_label, color='b')

    plt.fill_between(range(len(satisfactions_reference)), satisfactions_reference, alpha=0.3, color='r')
    plt.fill_between(range(len(satisfactions_reference)), satisfactions_target, alpha=0.3, color='b')
    plt.fill_between(range(len(satisfactions_reference)), satisfactions_both, color='g')

    plt.title(f"Cost satisfaction of PB instance {instance_id}:\n{reference_label} versus {target_label}", fontsize=16)
    plt.xlabel("Voters", fontsize=12)
    plt.ylabel("Cost satisfaction (normalised by budget limit)", fontsize=12)
    plt.legend(fontsize=14)
    plt.tight_layout()
    plt.savefig(f"plots/examples/{instance_id}_sat_{reference_rule}_{target_rule}.pdf")
    plt.close()


if __name__ == "__main__":
    results = "/Users/Pelle/OneDrive/Documents/MSc/Participatory Budgeting/git/data/results.csv"
    pabulibs = "/Users/Pelle/surfdrive/Shared/AmsterdamBB_clean/pabulib"
    data_dir = "/Users/Pelle/OneDrive/Documents/MSc/Participatory Budgeting/git/data"

    results_data = read_results(results, pabulibs)
    properties_data = read_properties_files(data_dir)

    examples = [522, 613, 644]
    for example in examples:
        plot_satisfaction_distribution(results_data, example,
                                       "Greedy", "GreedCost", "Completed iterated MES", r"MES$^{{*}{+}}$")
        plot_category_proportionality(results_data, example,
                                      "Greedy", "GreedCost", "Completed iterated MES", r"MES$^{{*}{+}}$")
        print_winners(results_data, example,
                      "Greedy", "GreedCost", "Completed iterated MES", r"MES$^{{*}{+}}$")

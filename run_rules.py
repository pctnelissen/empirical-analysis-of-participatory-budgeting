import csv
from os import listdir
from time import time

import pbvoting.instance.satisfaction as sat
from pbvoting.instance.pabulib import parse_pabulib
from pbvoting.rules.greedywelfare import greedy_welfare_approval
from pbvoting.rules.mes import method_of_equal_shares_approval
from pbvoting.rules.completion import completion_by_rule_resolute, completion_by_budget_increase
from pbvoting.instance.pbinstance import total_cost

if __name__ == "__main__":
    pabulib_directory = "/Users/Pelle/surfdrive/Shared/AmsterdamBB_clean/pabulib"
    output_file = "data/results.csv"

    def mes_cost(inst, prof):
        return method_of_equal_shares_approval(inst, prof, sat.COST_SATISFACTION(inst, prof))

    rules = [
        {
            "name": "Greedy",
            "call": lambda inst, prof: greedy_welfare_approval(inst, prof, sat.COST_SATISFACTION(inst, prof))
        },
        {
            "name": "MES",
            "call": mes_cost
        },
        {
            "name": "Completed MES",
            "call": lambda inst, prof: completion_by_rule_resolute(
                lambda inst1, prof1: results["MES"],
                lambda inst1, prof1, alloc: greedy_welfare_approval(inst1, prof1, sat.COST_SATISFACTION(inst1, prof1),
                                                                    initial_budget_allocation=alloc),
                inst, prof
            )
        },
        {
            "name": "Iterated MES",
            "call": lambda inst, prof: completion_by_budget_increase(mes_cost, inst, prof,
                                                                     budget_step=inst.budget_limit / 100)
        },
        {
            "name": "Completed iterated MES",
            "call": lambda inst, prof: completion_by_rule_resolute(
                lambda inst1, prof1: results["Iterated MES"],
                lambda inst1, prof1, alloc: greedy_welfare_approval(inst1, prof1, sat.COST_SATISFACTION(inst1, prof1),
                                                                    initial_budget_allocation=alloc),
                inst, prof
            )
        }
    ]

    max_str_len = 0
    for rule in rules:
        if len(rule["name"]) > max_str_len:
            max_str_len = len(rule["name"])

    with open(output_file, 'w') as file:
        writer = csv.writer(file, delimiter=';')
        header = ["Instance"]
        for rule in rules:
            header.append(rule["name"])
        writer.writerow(header)

    count = 0
    for pabulib_file in listdir(pabulib_directory):
        if pabulib_file == "588.pb" or pabulib_file[0] == '.':
            continue
        count += 1

        with open(output_file, 'a') as file:
            writer = csv.writer(file, delimiter=';')

            instance_id = int(pabulib_file[-6:-3])
            pabulib_file = pabulib_directory + '/' + pabulib_file

            instance, profile = parse_pabulib(pabulib_file)
            print(f"{count}/{len(listdir(pabulib_directory)) - 1}: "
                  f"instance: {instance_id}, projects: {len(instance)}, votes: {len(profile)}, "
                  f"budget: {int(instance.budget_limit)}")

            results = dict()
            results_str = []
            for rule in rules:
                start = time()
                result = rule["call"](instance, profile)
                end = time()

                results[rule["name"]] = result
                result_str = str(result).replace(' ', '')[1:-1]
                results_str.append(result_str)

                leftover = (instance.budget_limit - total_cost(result)) / instance.budget_limit
                print(f"{rule['name']}:{(max_str_len - len(rule['name'])) * ' '} {result_str} "
                      f"(leftover: {int(leftover * 100)}%, time: {int(end - start)}s)")

            writer.writerow([str(instance_id)] + results_str)
            print("")
